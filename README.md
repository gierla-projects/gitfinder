# GitFinder

## Description
**This is a small android app for allowing the user to search for a git repository on GitHub by using the REST Api.**

## Structure
The app is structured using some module and is bound together through gradle and hilt as dependency injection framework.

### UI
This module contains all ui related code. We are using the ReactiveComponents library to create small reactive ui elements that finally get combined into a screen ui component.
The screen components represent a whole screen an are used inside the app module as views for the related fragments. By using ReactiveComponents each screen exposes one state and one listener to update the views and listen to actions accordingly.

### Remote
This module contains all network related stuff. It contains all dtos and uses retrofit as networking library. It exposes a single interface `RestManager` (with the help of hilt to other modules) through which one can access the GitHub Api.
Here some Unit tests are implemented to test network related issues.

### Repository
This module contains some of our business logic like pagination and related. Here we would also combine remote and persistence layers if we would have a persistence layer. 

### buildSrc
This is a kotlin module containing information for our build. It is a centralized store for library version and so on.

### App
This is the main app module which contains android framework related stuff like activity, fragments, navigation, ...
It uses the persistence and ui module to build the ui and connect it to the api.
 

