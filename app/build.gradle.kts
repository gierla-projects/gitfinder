plugins {
    id(Dependencies.BuildPlugins.android.dependency)
    id(Dependencies.BuildPlugins.kotlinAndroid.dependency)
    id(Dependencies.BuildPlugins.kapt.dependency)
    id(Dependencies.BuildPlugins.gradleVersion.dependency)
    id(Dependencies.BuildPlugins.hilt.dependency)
    id(Dependencies.BuildPlugins.safeArgs.dependency)
    id("kotlin-android")
}

android {

    compileSdkVersion(Config.AndroidSdk.compileSdkVersion)

    defaultConfig {
        applicationId = Config.AndroidSdk.App.applicationId
        versionCode = Config.AndroidSdk.App.versionNumber
        versionName = Config.AndroidSdk.App.versionName

        minSdkVersion(Config.AndroidSdk.minSdkVersion)
        targetSdkVersion(Config.AndroidSdk.targetSdkVersion)

        buildConfigField("String", "REST_SERVER_URL", "\"https://api.github.com/\"")

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        named("release") {
            isMinifyEnabled = true
            isShrinkResources = true
        }
        named("debug") {
            debuggable(true)
            isMinifyEnabled = false
            isShrinkResources = false
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility =  JavaVersion.VERSION_1_8
    }

}

dependencies {
    // Local libraries
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Kotlin
    implementation(Dependencies.JetBrains.kotlin)

    // Concurrent Coding
    implementation(Dependencies.JetBrains.coroutines)
    implementation(Dependencies.JetBrains.coroutinesAndroid)
    implementation(Dependencies.AndroidX.lifecycleViewScope)

    // AndroidX Support Libraries
    implementation(Dependencies.AndroidX.core)
    implementation(Dependencies.AndroidX.appCompat)
    implementation(Dependencies.AndroidX.constraintLayout)

    // Navigation
    implementation(Dependencies.AndroidX.navigationFragment)
    implementation(Dependencies.AndroidX.navigationUi)

    // Material Design Components
    implementation(Dependencies.Google.material)

    // DI
    implementation(Dependencies.Google.hilt)
    kapt(Dependencies.Google.hiltCompiler)

    // Testing
    testImplementation(Dependencies.Other.jUnit)
    androidTestImplementation(Dependencies.AndroidX.androidJUnit)
    androidTestImplementation(Dependencies.AndroidX.espresso)

    // Repository module
    implementation(project(":repository"))

    // Ui module
    implementation(project(":ui"))
}