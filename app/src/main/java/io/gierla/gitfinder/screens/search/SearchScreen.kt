package io.gierla.gitfinder.screens.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import io.gierla.gitfinder.screens.search.helper.ActivityCallback
import io.gierla.gitfinder.ui.screens.SearchScreen
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class SearchScreen : Fragment(), ActivityCallback {

    private val viewModel by viewModels<SearchViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return SearchScreen(container!!.context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.setActivityCallback(this)

        (getView() as? SearchScreen)?.let { searchScreen ->

            searchScreen.setActionListener(viewModel::handleAction)

            viewModel
                .stateDispatcher
                .onEach { state ->
                    searchScreen.updateState { state }
                }
                .launchIn(lifecycleScope)

        }
    }

    override fun navigate(destination: NavDirections) {
        findNavController().navigate(destination)
    }
}