package io.gierla.gitfinder.screens.search.helper

import androidx.navigation.NavDirections

interface ActivityCallback {
    fun navigate(destination: NavDirections)
}