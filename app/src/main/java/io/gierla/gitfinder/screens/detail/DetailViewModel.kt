package io.gierla.gitfinder.screens.detail

import android.content.Context
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import io.gierla.gitfinder.R
import io.gierla.gitfinder.helper.SuperViewModel
import io.gierla.gitfinder.remote.managers.helper.ApiResponse
import io.gierla.gitfinder.repository.repositories.github.GitHubRepo
import io.gierla.gitfinder.screens.detail.helper.ActivityCallback
import io.gierla.gitfinder.ui.components.screen_error.ScreenError
import io.gierla.gitfinder.ui.screens.DetailScreen
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class DetailViewModel @Inject constructor(
    private val gitHubRepo: GitHubRepo,
    // It is fine to use the application context inside a ViewModel because it lives for the whole application lifecycle and here we use it for string related reason and not für UI manipulation
    @ApplicationContext private val context: Context
) : SuperViewModel<DetailScreen.ViewState>(DetailScreen.ViewState()) {

    private var activityCallback: ActivityCallback? = null

    fun loadRepo(id: Long) {
        viewModelScope.launch {
            when (val apiResponse = gitHubRepo.getById(id)) {
                is ApiResponse.Error -> {
                    state = state.copy(
                        hasError = true,
                        screenErrorState = state.screenErrorState.copy(
                            error = apiResponse.message ?: context.getString(R.string.generic_error)
                        )
                    )
                }
                is ApiResponse.NetworkError -> {
                    state = state.copy(
                        hasError = true,
                        screenErrorState = state.screenErrorState.copy(
                            error = apiResponse.error.localizedMessage ?: context.getString(R.string.generic_error)
                        )
                    )
                }
                is ApiResponse.Success -> {
                    state = state.copy(
                        title = apiResponse.value?.name ?: "",
                        url = apiResponse.value?.url,
                        subTitle = apiResponse.value?.fullName ?: "",
                        description = apiResponse.value?.description ?: "",
                        starBadgeState = state.starBadgeState.copy(
                            text = apiResponse.value?.stars?.toString() ?: "0"
                        ),
                        langBadgeState = state.langBadgeState.copy(
                            text = apiResponse.value?.lang ?: ""
                        ),
                        profileImage = apiResponse.value?.owner?.profileImage
                    )
                }
            }
        }
    }

    fun handleAction(action: DetailScreen.ViewAction) {
        when (action) {
            is DetailScreen.ViewAction.ScreenErrorAction -> {
                when (action.screenErrorAction) {
                    ScreenError.ViewAction.DismissAction -> {
                        state = state.copy(
                            hasError = false
                        )
                    }
                }
            }
            DetailScreen.ViewAction.BackAction -> {
                activityCallback?.navigateUp()
            }
            is DetailScreen.ViewAction.OpenBrowserAction -> {
                activityCallback?.openBrowser(action.url)
            }
        }
    }

    fun setActivityCallback(callback: ActivityCallback?) {
        activityCallback = callback
    }

}