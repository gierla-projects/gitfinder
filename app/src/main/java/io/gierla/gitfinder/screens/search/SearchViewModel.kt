package io.gierla.gitfinder.screens.search

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.filter
import androidx.paging.map
import dagger.hilt.android.lifecycle.HiltViewModel
import io.gierla.gitfinder.helper.SuperViewModel
import io.gierla.gitfinder.repository.repositories.github.GitHubRepo
import io.gierla.gitfinder.screens.search.helper.ActivityCallback
import io.gierla.gitfinder.ui.components.search_list.SearchList
import io.gierla.gitfinder.ui.components.repo_item.RepoItem
import io.gierla.gitfinder.ui.components.info_badge.InfoBadge
import io.gierla.gitfinder.ui.components.screen_error.ScreenError
import io.gierla.gitfinder.ui.screens.SearchScreen
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class SearchViewModel @Inject constructor(
    private val gitHubRepo: GitHubRepo
) : SuperViewModel<SearchScreen.ViewState>(SearchScreen.ViewState()) {

    private var activityCallback: ActivityCallback? = null

    private fun search(query: String) {
        if (query.isBlank()) {
            state = state.copy(
                searchListState = state.searchListState.copy(
                    repoItems = PagingData.empty()
                ),
                isEmpty = true
            )
        } else {
            gitHubRepo
                .search(query)
                .cachedIn(viewModelScope)
                .onEach { searchResult ->
                    state = state.copy(
                        searchListState = state.searchListState.copy(
                            repoItems = searchResult
                                .filter {
                                    it.id != null
                                }
                                .map {
                                    RepoItem.ViewState(
                                        id = it.id ?: 0L,
                                        name = it.name ?: "",
                                        fullName = it.fullName ?: "",
                                        starBadgeState = InfoBadge.ViewState(
                                            text = it.stars.toString()
                                        ),
                                        langBadgeState = InfoBadge.ViewState(
                                            text = it.lang ?: ""
                                        )
                                    )
                                }
                        )
                    )
                }
                .launchIn(viewModelScope)
        }
    }

    fun handleAction(action: SearchScreen.ViewAction) {
        when (action) {
            is SearchScreen.ViewAction.SearchAction -> {
                search(action.search)
            }
            is SearchScreen.ViewAction.SearchListAction -> {
                when (val searchListAction = action.searchListAction) {
                    is SearchList.ViewAction.LoadErrorAction -> {
                        state = state.copy(
                            isLoading = searchListAction.isLoading,
                            isEmpty = searchListAction.isEmpty,
                            hasError = searchListAction.hasError,
                            screenErrorState = state.screenErrorState.copy(
                                error = searchListAction.error
                            )
                        )
                    }
                    is SearchList.ViewAction.RepoItemAction -> {
                        when (val repoItemAction = searchListAction.repoItemAction) {
                            is RepoItem.ViewAction.ClickAction -> {
                                activityCallback?.navigate(SearchScreenDirections.actionSearchScreenToDetailScreen(repoItemAction.id))
                            }
                        }
                    }
                }
            }
            is SearchScreen.ViewAction.ScreenErrorAction -> {
                when (action.screenErrorAction) {
                    ScreenError.ViewAction.DismissAction -> {
                        state = state.copy(
                            hasError = false
                        )
                    }
                }
            }
        }
    }

    fun setActivityCallback(callback: ActivityCallback?) {
        activityCallback = callback
    }

}