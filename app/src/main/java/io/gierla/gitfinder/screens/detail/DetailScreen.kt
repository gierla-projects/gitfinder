package io.gierla.gitfinder.screens.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import io.gierla.gitfinder.screens.detail.helper.ActivityCallback
import io.gierla.gitfinder.ui.screens.DetailScreen
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class DetailScreen : Fragment(), ActivityCallback {

    private val args: DetailScreenArgs by navArgs()

    private val viewModel by viewModels<DetailViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return DetailScreen(container!!.context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.setActivityCallback(this)

        (getView() as? DetailScreen)?.let { detailScreen ->

            detailScreen.setActionListener(viewModel::handleAction)

            viewModel
                .stateDispatcher
                .onEach { state ->
                    detailScreen.updateState { state }
                }
                .launchIn(lifecycleScope)

        }

        viewModel.loadRepo(args.repoId)
    }

    override fun navigateUp() {
        findNavController().navigateUp()
    }

    override fun openBrowser(url: String) {
        val intent = Intent(Intent.ACTION_VIEW).apply { data = Uri.parse(url) }
        try {
            startActivity(intent)
        } catch (e: Exception) {
            Log.e("DetailScreen", "Could not open url: ${e.localizedMessage}")
        }
    }
}