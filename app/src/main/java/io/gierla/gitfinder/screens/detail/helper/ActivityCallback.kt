package io.gierla.gitfinder.screens.detail.helper

interface ActivityCallback {
    fun navigateUp()
    fun openBrowser(url: String)
}