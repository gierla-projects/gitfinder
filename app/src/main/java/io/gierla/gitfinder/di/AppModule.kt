package io.gierla.gitfinder.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.gierla.gitfinder.BuildConfig
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    @Named("rest_server_url")
    fun provideSocketServerUrl(): String {
        return BuildConfig.REST_SERVER_URL
    }

}