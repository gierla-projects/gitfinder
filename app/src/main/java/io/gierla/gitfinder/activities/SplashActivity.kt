package io.gierla.gitfinder.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Use this place to initialize mandatory resources

        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}