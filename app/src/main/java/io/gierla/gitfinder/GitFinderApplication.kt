package io.gierla.gitfinder

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GitFinderApplication : Application() {
}