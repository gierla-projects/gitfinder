package io.gierla.gitfinder.helper

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

@ExperimentalCoroutinesApi
abstract class SuperViewModel<T>(initialState: T) : ViewModel() {

    protected var state = initialState
        set(value) {
            field = value
            _stateDispatcher.value = field
        }

    private val _stateDispatcher = MutableStateFlow(state)
    val stateDispatcher: StateFlow<T> get() = _stateDispatcher

}