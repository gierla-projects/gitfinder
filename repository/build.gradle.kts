plugins {
    id(Dependencies.BuildPlugins.androidLibrary.dependency)
    id(Dependencies.BuildPlugins.kotlinAndroid.dependency)
    id(Dependencies.BuildPlugins.kapt.dependency)
    id(Dependencies.BuildPlugins.hilt.dependency)
    id(Dependencies.BuildPlugins.gradleVersion.dependency)
}

android {
    compileSdkVersion(Config.AndroidSdk.compileSdkVersion)

    defaultConfig {
        minSdkVersion(Config.AndroidSdk.minSdkVersion)
        targetSdkVersion(Config.AndroidSdk.targetSdkVersion)

        versionCode = Config.AndroidSdk.Repository.versionNumber
        versionName = Config.AndroidSdk.Repository.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    compileOptions {
        targetCompatibility = JavaVersion.VERSION_1_8
        sourceCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    // Local libraries
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Kotlin
    implementation(Dependencies.JetBrains.kotlin)

    // Paging
    api(Dependencies.AndroidX.paging)

    // DI
    implementation(Dependencies.Google.hilt)
    kapt(Dependencies.Google.hiltCompiler)

    // Testing
    testImplementation(Dependencies.Other.jUnit)
    androidTestImplementation(Dependencies.AndroidX.androidJUnit)
    androidTestImplementation(Dependencies.AndroidX.espresso)

    // Remote module
    api(project(":remote"))
}