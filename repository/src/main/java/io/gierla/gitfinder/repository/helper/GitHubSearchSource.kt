package io.gierla.gitfinder.repository.helper

import androidx.paging.PagingSource
import androidx.paging.PagingState
import io.gierla.gitfinder.remote.dtos.response.Repository
import io.gierla.gitfinder.remote.managers.helper.ApiResponse
import io.gierla.gitfinder.remote.managers.rest_manager.RestManager
import io.gierla.gitfinder.repository.repositories.github.GitHubRepoImpl.Companion.PAGE_SIZE

internal class GitHubSearchSource(
    private val restManager: RestManager,
    private val query: String
) : PagingSource<Long, Repository>() {

    companion object {
        private const val START_POS = 1L
    }

    override fun getRefreshKey(state: PagingState<Long, Repository>): Long? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1) ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Long>): LoadResult<Long, Repository> {
        val position = params.key ?: START_POS

        return when (val items = restManager.search(query, position, params.loadSize)) {
            is ApiResponse.Error -> LoadResult.Error(Exception(items.message))
            is ApiResponse.NetworkError -> LoadResult.Error(items.error)
            is ApiResponse.Success -> LoadResult.Page(
                data = items.value?.items ?: emptyList(),
                nextKey = if (items.value?.items?.isNullOrEmpty() == true) null else (position + params.loadSize / PAGE_SIZE),
                prevKey = if (position == START_POS) null else (position - 1)
            )
        }
    }
}