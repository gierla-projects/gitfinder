package io.gierla.gitfinder.repository.repositories.github

import androidx.paging.PagingData
import io.gierla.gitfinder.remote.dtos.response.Repository
import io.gierla.gitfinder.remote.managers.helper.ApiResponse
import kotlinx.coroutines.flow.Flow

interface GitHubRepo {

    fun search(query: String): Flow<PagingData<Repository>>

    suspend fun getById(id: Long): ApiResponse<Repository>

}