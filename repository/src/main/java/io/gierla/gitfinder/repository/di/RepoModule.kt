package io.gierla.gitfinder.repository.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.gierla.gitfinder.remote.managers.rest_manager.RestManager
import io.gierla.gitfinder.repository.repositories.github.GitHubRepo
import io.gierla.gitfinder.repository.repositories.github.GitHubRepoImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepoModule {

    @Singleton
    @Provides
    fun provideGitHubRepo(
        restManager: RestManager
    ): GitHubRepo {
        return GitHubRepoImpl(restManager)
    }

}