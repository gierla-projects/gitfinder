package io.gierla.gitfinder.repository.repositories.github

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import io.gierla.gitfinder.remote.dtos.response.Repository
import io.gierla.gitfinder.remote.managers.helper.ApiResponse
import io.gierla.gitfinder.remote.managers.rest_manager.RestManager
import io.gierla.gitfinder.repository.helper.GitHubSearchSource
import kotlinx.coroutines.flow.Flow

internal class GitHubRepoImpl(
    private val restManager: RestManager
) : GitHubRepo {

    companion object {
        const val PAGE_SIZE = 20
    }

    override fun search(query: String): Flow<PagingData<Repository>> = Pager(
        config = PagingConfig(
            pageSize = PAGE_SIZE,
            enablePlaceholders = false
        )
    ) {
        GitHubSearchSource(
            restManager = restManager,
            query = query
        )
    }.flow

    override suspend fun getById(id: Long): ApiResponse<Repository> {
        return restManager.getById(id)
    }
}