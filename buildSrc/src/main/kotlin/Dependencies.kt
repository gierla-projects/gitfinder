import helper.VersionDependency

object Dependencies {

    private const val kotlinVersion = "1.5.0-RC"

    object BuildPlugins {
        val androidLibrary = VersionDependency("com.android.library")
        val android = VersionDependency("com.android.application", "4.1.3")
        val hilt = VersionDependency("dagger.hilt.android.plugin", "2.34.1-beta")
        val kotlinAndroid = VersionDependency("org.jetbrains.kotlin.android", kotlinVersion)
        val gradleVersion = VersionDependency("com.github.ben-manes.versions", "0.38.0")
        val kapt = VersionDependency("org.jetbrains.kotlin.kapt", kotlinVersion)
        val safeArgs = VersionDependency("androidx.navigation.safeargs.kotlin", "2.3.5")
    }

    object JetBrains {
        val kotlin = VersionDependency("org.jetbrains.kotlin:kotlin-stdlib", kotlinVersion).get()

        val coroutines = VersionDependency("org.jetbrains.kotlinx:kotlinx-coroutines-core", "1.4.2").get()
        val coroutinesAndroid = VersionDependency("org.jetbrains.kotlinx:kotlinx-coroutines-android", "1.4.2").get()
    }

    object AndroidX {
        val core = VersionDependency("androidx.core:core", "1.6.0-alpha02").get()
        val appCompat = VersionDependency("androidx.appcompat:appcompat", "1.3.0-rc01").get()
        val constraintLayout = VersionDependency("androidx.constraintlayout:constraintlayout", "2.1.0-beta01").get()
        val androidJUnit = VersionDependency("androidx.test.ext:junit", "1.1.3-alpha05").get()
        val espresso = VersionDependency("androidx.test.espresso:espresso-core", "3.4.0-alpha05").get()

        val navigationFragment = VersionDependency("androidx.navigation:navigation-fragment-ktx", "2.3.5").get()
        val navigationUi = VersionDependency("androidx.navigation:navigation-ui-ktx", "2.3.5").get()

        val lifecycleViewScope = VersionDependency("androidx.lifecycle:lifecycle-viewmodel-ktx", "2.4.0-alpha01").get()

        val recyclerView = VersionDependency("androidx.recyclerview:recyclerview", "1.2.0").get()

        val paging = VersionDependency("androidx.paging:paging-runtime", "3.0.0-beta03").get()
    }

    object Google {
        val material = VersionDependency("com.google.android.material:material", "1.4.0-alpha02").get()
        val gson = VersionDependency("com.google.code.gson:gson", "2.8.6").get()
        val hilt = VersionDependency("com.google.dagger:hilt-android", "2.34.1-beta").get()
        val hiltCompiler = VersionDependency("com.google.dagger:hilt-compiler", "2.34.1-beta").get()
    }

    object SquareUp {
        val retrofit = VersionDependency("com.squareup.retrofit2:retrofit", "2.9.0").get()
        val retrofitGson = VersionDependency("com.squareup.retrofit2:converter-gson", "2.9.0").get()
        val okHttpLoggingInterceptor = VersionDependency("com.squareup.okhttp3:logging-interceptor", "5.0.0-alpha.2").get()
        val mockWebServer = VersionDependency("com.squareup.okhttp3:mockwebserver", "4.7.2").get()
    }

    object Gierla {
        val reactiveComponents = VersionDependency("io.gierla.reactivecomponents:core", "0.0.25").get()
        val reactiveComponentsAnnotations = VersionDependency("io.gierla.reactivecomponents:annotations", "0.0.25").get()
    }

    object Other {
        val jUnit = VersionDependency("junit:junit", "4.13.2").get()
        val flowBinding = VersionDependency("io.github.reactivecircus.flowbinding:flowbinding-android", "1.0.0-beta02").get()
        val coil = VersionDependency("io.coil-kt:coil", "1.2.0").get()
    }

}