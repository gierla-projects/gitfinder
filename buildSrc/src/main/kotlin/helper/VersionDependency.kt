package helper

class VersionDependency(val dependency: String, val version: String? = null) {

    fun get(): String = toString()

    override fun toString(): String {
        return if(version == null) {
            dependency
        } else {
            "$dependency:$version"
        }
    }

}