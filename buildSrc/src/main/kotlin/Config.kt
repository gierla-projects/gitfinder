object Config {

    object AndroidSdk {

        const val compileSdkVersion = 29
        const val minSdkVersion = 16
        const val targetSdkVersion = 29

        object App : AndroidModule {
            const val applicationId = "io.gierla.gitfinder"

            override val versionNumber = 1
            override val versionName = "1.0.0"
        }

        object Remote : AndroidModule {
            override val versionNumber = 1
            override val versionName = "1.0.0"
        }

        object Ui : AndroidModule {
            override val versionNumber = 1
            override val versionName = "1.0.0"
        }

        object Repository : AndroidModule {
            override val versionNumber = 1
            override val versionName = "1.0.0"
        }

    }

    private interface AndroidModule {
        val versionNumber: Int
        val versionName: String
    }

}