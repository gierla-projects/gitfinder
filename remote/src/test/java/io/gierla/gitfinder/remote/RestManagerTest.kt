package io.gierla.gitfinder.remote

import io.gierla.gitfinder.remote.di.RemoteModule
import io.gierla.gitfinder.remote.managers.helper.ApiResponse
import io.gierla.gitfinder.remote.managers.rest_manager.RestManager
import io.gierla.gitfinder.remote.managers.rest_manager.RestManagerImpl
import io.gierla.gitfinder.remote.managers.service_manager.ServiceManagerImpl
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.SocketPolicy
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.InputStreamReader
import java.net.HttpURLConnection

class RestManagerTest {

    private val mockWebServer = MockWebServer()

    private lateinit var restManager: RestManager

    @Before
    fun setup() {
        val httpLoggingInterceptor = RemoteModule.provideHttpLoggingInterceptor()
        val okHttpClient = RemoteModule.provideOkHttpClient(httpLoggingInterceptor)
        val gson = RemoteModule.provideGson()
        val retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        restManager = RestManagerImpl(ServiceManagerImpl(retrofit), gson)
    }

    @Test
    fun `should fetch repo-list correctly given 200 response`() {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(readContent("success_search.json"))
        mockWebServer.enqueue(response)

        runBlocking {
            val res = restManager.search("react", 1, 20)

            assertTrue(res is ApiResponse.Success)
            assertEquals(20, (res as? ApiResponse.Success)?.value?.items?.size)
            assertEquals("JavaScript", (res as? ApiResponse.Success)?.value?.items?.get(0)?.lang)
            assertEquals("facebook/react", (res as? ApiResponse.Success)?.value?.items?.get(0)?.fullName)
        }
    }

    @Test
    fun `should return network error given no connection`() {
        val response = MockResponse()
            .setSocketPolicy(SocketPolicy.DISCONNECT_AT_START);
        mockWebServer.enqueue(response)

        runBlocking {
            val res = restManager.search("react", 1, 20)

            assertTrue(res is ApiResponse.NetworkError)
            assertTrue((res as? ApiResponse.NetworkError)?.error?.localizedMessage?.startsWith("Failed to connect to") == true)
        }
    }

    @Test
    fun `should return invalid error given 422 response`() {
        val response = MockResponse()
            .setResponseCode(422)
            .setBody(readContent("failed_search.json"))
        mockWebServer.enqueue(response)

        runBlocking {
            val res = restManager.search("", 1, 20)

            assertTrue(res is ApiResponse.Error)
            assertEquals(422, (res as? ApiResponse.Error)?.errorCode)
            assertEquals("Validation Failed", (res as? ApiResponse.Error)?.message)
        }
    }

    @Test
    fun `should fetch repo correctly given 200 response`() {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(readContent("success_repo.json"))
        mockWebServer.enqueue(response)

        runBlocking {
            val res = restManager.getById(10270250)

            assertTrue(res is ApiResponse.Success)
            assertEquals(10270250L, (res as? ApiResponse.Success)?.value?.id)
            assertEquals("facebook/react", (res as? ApiResponse.Success)?.value?.fullName)
        }
    }

    @Test
    fun `should return not found error given 404 response`() {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_NOT_FOUND)
            .setBody(readContent("failed_repo.json"))
        mockWebServer.enqueue(response)

        runBlocking {
            val res = restManager.getById(10270250)

            assertTrue(res is ApiResponse.Error)
            assertEquals(404, (res as? ApiResponse.Error)?.errorCode)
            assertEquals("Not Found", (res as? ApiResponse.Error)?.message)
        }
    }

    private fun readContent(path: String): String {
        return InputStreamReader(this.javaClass.classLoader?.getResourceAsStream(path)).use {
            it.readText()
        }
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

}