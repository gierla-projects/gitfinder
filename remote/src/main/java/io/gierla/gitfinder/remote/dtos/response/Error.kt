package io.gierla.gitfinder.remote.dtos.response

import com.google.gson.annotations.SerializedName

data class Error(

    @SerializedName("message")
    val message: String? = null

)
