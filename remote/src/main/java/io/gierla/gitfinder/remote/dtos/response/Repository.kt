package io.gierla.gitfinder.remote.dtos.response

import com.google.gson.annotations.SerializedName

data class Repository(

    @SerializedName("id")
    val id: Long? = null,

    @SerializedName("name")
    val name: String? = null,

    @SerializedName("full_name")
    val fullName: String? = null,

    @SerializedName("stargazers_count")
    val stars: Int = 0,

    @SerializedName("language")
    val lang: String? = null,

    @SerializedName("html_url")
    val url: String? = null,

    @SerializedName("description")
    val description: String? = null,

    @SerializedName("owner")
    val owner: User? = null

)
