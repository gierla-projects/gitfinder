package io.gierla.gitfinder.remote.dtos.response

import com.google.gson.annotations.SerializedName

data class Search(

    @SerializedName("total_count")
    val total: Int = 0,

    @SerializedName("items")
    val items: List<Repository> = emptyList()

)
