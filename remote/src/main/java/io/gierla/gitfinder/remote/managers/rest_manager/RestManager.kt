package io.gierla.gitfinder.remote.managers.rest_manager

import io.gierla.gitfinder.remote.dtos.response.Search
import io.gierla.gitfinder.remote.dtos.response.Repository
import io.gierla.gitfinder.remote.managers.helper.ApiResponse

interface RestManager {

    suspend fun search(query: String, page: Long, itemsPerPage: Int): ApiResponse<Search>

    suspend fun getById(id: Long): ApiResponse<Repository>

}