package io.gierla.gitfinder.remote.managers.apis

import io.gierla.gitfinder.remote.dtos.response.Repository
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

internal interface GitHubRepoApi {

    @Headers(
        value = [
            "Accept: application/vnd.github.v3+json",
            "User-Agent: GitFinder"
        ]
    )
    @GET("repositories/{id}")
    suspend fun getById(
        @Path("id") id: Long
    ): Repository

}