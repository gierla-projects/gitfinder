package io.gierla.gitfinder.remote.managers.rest_manager

import com.google.gson.Gson
import io.gierla.gitfinder.remote.dtos.response.Error
import io.gierla.gitfinder.remote.dtos.response.Search
import io.gierla.gitfinder.remote.dtos.response.Repository
import io.gierla.gitfinder.remote.managers.helper.ApiResponse
import io.gierla.gitfinder.remote.managers.service_manager.ServiceManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException

internal class RestManagerImpl(
    private val serviceManager: ServiceManager,
    private val gson: Gson
) : RestManager {

    override suspend fun search(query: String, page: Long, itemsPerPage: Int): ApiResponse<Search> = withContext(Dispatchers.IO) {
        wrapCall { serviceManager.gitHubSearchApi.search(query, page, itemsPerPage) }
    }

    override suspend fun getById(id: Long): ApiResponse<Repository> = withContext(Dispatchers.IO) {
        wrapCall { serviceManager.gitHubRepoApi.getById(id) }
    }

    private suspend fun <T> wrapCall(call: suspend () -> T): ApiResponse<T> {
        return try {
            ApiResponse.Success(call.invoke())
        } catch (throwable: Throwable) {
            when (throwable) {
                is HttpException -> {
                    val code = throwable.code()
                    val errorResponse = convertErrorBody(throwable)
                    ApiResponse.Error(message = errorResponse.message, errorCode = code)
                }
                else -> ApiResponse.NetworkError(throwable)
            }
        }
    }

    private fun convertErrorBody(throwable: HttpException): Error {
        return try {
            throwable.response()?.errorBody()?.source()?.let {
                try {
                    gson.fromJson(it.readUtf8(), Error::class.java)
                } catch (e: Exception) {
                    null
                }
            } ?: Error(null)
        } catch (exception: Exception) {
            Error(null)
        }
    }

}