package io.gierla.gitfinder.remote.managers.service_manager

import io.gierla.gitfinder.remote.managers.apis.GitHubRepoApi
import io.gierla.gitfinder.remote.managers.apis.GitHubSearchApi

internal interface ServiceManager {

    val gitHubSearchApi: GitHubSearchApi

    val gitHubRepoApi: GitHubRepoApi

}