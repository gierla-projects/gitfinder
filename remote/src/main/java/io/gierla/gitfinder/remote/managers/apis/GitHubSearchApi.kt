package io.gierla.gitfinder.remote.managers.apis

import io.gierla.gitfinder.remote.dtos.response.Search
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

internal interface GitHubSearchApi {

    @Headers(
        value = [
            "Accept: application/vnd.github.v3+json",
            "User-Agent: GitFinder"
        ]
    )
    @GET("search/repositories")
    suspend fun search(
        @Query("q") query: String,
        @Query("page") page: Long,
        @Query("per_page") itemsPerPage: Int
    ): Search

}