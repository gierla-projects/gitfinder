package io.gierla.gitfinder.remote.managers.helper

sealed class ApiResponse<out T> {
    data class Success<out T>(val value: T?) : ApiResponse<T>()
    data class Error(val message: String? = null, val errorCode: Int? = null) : ApiResponse<Nothing>()
    data class NetworkError(val error: Throwable) : ApiResponse<Nothing>()
}
