package io.gierla.gitfinder.remote.managers.service_manager

import io.gierla.gitfinder.remote.managers.apis.GitHubRepoApi
import io.gierla.gitfinder.remote.managers.apis.GitHubSearchApi
import retrofit2.Retrofit
import retrofit2.create

internal class ServiceManagerImpl(
    private val retrofit: Retrofit
) : ServiceManager {

    override val gitHubSearchApi: GitHubSearchApi by lazy { retrofit.create() }

    override val gitHubRepoApi: GitHubRepoApi by lazy { retrofit.create() }

}