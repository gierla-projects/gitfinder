package io.gierla.gitfinder.remote.dtos.response

import com.google.gson.annotations.SerializedName

data class User(

    @SerializedName("avatar_url")
    val profileImage: String? = null

)
