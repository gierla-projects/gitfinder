package io.gierla.gitfinder.remote.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.gierla.gitfinder.remote.managers.rest_manager.RestManager
import io.gierla.gitfinder.remote.managers.rest_manager.RestManagerImpl
import io.gierla.gitfinder.remote.managers.service_manager.ServiceManagerImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RemoteModule {

    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC)
    }

    fun provideOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient
            .Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }

    fun provideGson(): Gson {
        return GsonBuilder().create()
    }

    private fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson,
        serverUrl: String
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(serverUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Singleton
    @Provides
    fun provideRestManager(
        @Named("rest_server_url") serverUrl: String
    ): RestManager {
        val httpLoggingInterceptor = provideHttpLoggingInterceptor()
        val okHttpClient = provideOkHttpClient(httpLoggingInterceptor)
        val gson = provideGson()
        val retrofit = provideRetrofit(okHttpClient, gson, serverUrl)

        return RestManagerImpl(
            ServiceManagerImpl(retrofit),
            gson
        )
    }

}