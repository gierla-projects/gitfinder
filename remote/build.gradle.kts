plugins {
    id(Dependencies.BuildPlugins.androidLibrary.dependency)
    id(Dependencies.BuildPlugins.kotlinAndroid.dependency)
    id(Dependencies.BuildPlugins.kapt.dependency)
    id(Dependencies.BuildPlugins.hilt.dependency)
    id(Dependencies.BuildPlugins.gradleVersion.dependency)
}

android {
    compileSdkVersion(Config.AndroidSdk.compileSdkVersion)

    defaultConfig {
        minSdkVersion(Config.AndroidSdk.minSdkVersion)
        targetSdkVersion(Config.AndroidSdk.targetSdkVersion)

        versionCode = Config.AndroidSdk.Remote.versionNumber
        versionName = Config.AndroidSdk.Remote.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    compileOptions {
        targetCompatibility = JavaVersion.VERSION_1_8
        sourceCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    // Local libraries
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Kotlin
    implementation(Dependencies.JetBrains.kotlin)

    // Concurrent Coding
    implementation(Dependencies.JetBrains.coroutines)

    // Networking
    implementation(Dependencies.SquareUp.retrofit)
    implementation(Dependencies.SquareUp.retrofitGson)
    implementation(Dependencies.SquareUp.okHttpLoggingInterceptor)

    // Object-Json mapping
    implementation(Dependencies.Google.gson)

    // DI
    implementation(Dependencies.Google.hilt)
    kapt(Dependencies.Google.hiltCompiler)

    // Testing
    testImplementation(Dependencies.Other.jUnit)
    androidTestImplementation(Dependencies.AndroidX.androidJUnit)
    androidTestImplementation(Dependencies.AndroidX.espresso)
    testImplementation(Dependencies.SquareUp.mockWebServer)
}