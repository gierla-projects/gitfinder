import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id(Dependencies.BuildPlugins.kotlinAndroid.dependency) version Dependencies.BuildPlugins.kotlinAndroid.version apply false
    id(Dependencies.BuildPlugins.kapt.dependency) version Dependencies.BuildPlugins.kapt.version apply false

    id(Dependencies.BuildPlugins.android.dependency) version Dependencies.BuildPlugins.android.version apply false
    id(Dependencies.BuildPlugins.hilt.dependency) version Dependencies.BuildPlugins.hilt.version apply false

    id(Dependencies.BuildPlugins.gradleVersion.dependency) version Dependencies.BuildPlugins.gradleVersion.version apply false

    id(Dependencies.BuildPlugins.safeArgs.dependency) version Dependencies.BuildPlugins.safeArgs.version apply false
}

allprojects {
    repositories {
        google()
        jcenter()
    }
    tasks.withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_1_8.toString()
        }
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
buildscript {
    val kotlin_version by extra("1.4.32")
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")
    }
}
