plugins {
    id(Dependencies.BuildPlugins.androidLibrary.dependency)
    id(Dependencies.BuildPlugins.kotlinAndroid.dependency)
    id(Dependencies.BuildPlugins.gradleVersion.dependency)
    id(Dependencies.BuildPlugins.kapt.dependency)
}

android {
    compileSdkVersion(Config.AndroidSdk.compileSdkVersion)

    defaultConfig {
        minSdkVersion(Config.AndroidSdk.minSdkVersion)
        targetSdkVersion(Config.AndroidSdk.targetSdkVersion)

        versionCode = Config.AndroidSdk.Ui.versionNumber
        versionName = Config.AndroidSdk.Ui.versionName

        vectorDrawables.useSupportLibrary = true

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildFeatures {
        viewBinding = true
    }

    compileOptions {
        targetCompatibility = JavaVersion.VERSION_1_8
        sourceCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    // Local libraries
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Kotlin
    implementation(Dependencies.JetBrains.kotlin)

    // AndroidX Support Libraries
    implementation(Dependencies.AndroidX.core)
    implementation(Dependencies.AndroidX.appCompat)
    implementation(Dependencies.AndroidX.constraintLayout)
    implementation(Dependencies.AndroidX.recyclerView)

    // Paging
    implementation(Dependencies.AndroidX.paging)

    // Reactive view extensions
    implementation(Dependencies.Other.flowBinding)

    // Image loading
    implementation(Dependencies.Other.coil)

    // Reactive compound views
    api(Dependencies.Gierla.reactiveComponents)
    kapt(Dependencies.Gierla.reactiveComponentsAnnotations)

    // Testing
    testImplementation(Dependencies.Other.jUnit)
    androidTestImplementation(Dependencies.AndroidX.androidJUnit)
    androidTestImplementation(Dependencies.AndroidX.espresso)
}