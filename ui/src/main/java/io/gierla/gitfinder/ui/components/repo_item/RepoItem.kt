package io.gierla.gitfinder.ui.components.repo_item

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import io.gierla.gitfinder.ui.R
import io.gierla.gitfinder.ui.components.info_badge.InfoBadge
import io.gierla.gitfinder.ui.databinding.RepoItemBinding
import io.gierla.gitfinder.ui.helper.ReactiveConstraintLayout
import io.gierla.rccore.annotations.ReactiveComponent
import io.gierla.rccore.main.action.Action
import io.gierla.rccore.main.state.State
import io.gierla.rccore.views.view.Structure
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import reactivecircus.flowbinding.android.view.clicks

@ExperimentalCoroutinesApi
@ReactiveComponent
class RepoItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ReactiveConstraintLayout<RepoItem.ViewState, RepoItem.ViewAction, RepoItem.ViewStructure>(
    ViewState(),
    context,
    attrs,
    defStyleAttr
) {

    private var binding: RepoItemBinding

    init {
        inflate(context, R.layout.repo_item, this)

        binding = RepoItemBinding.bind(rootView)

        setViewStructure {
            object : ViewStructure {
                override val root: ConstraintLayout = this@RepoItem
                override val name: TextView = binding.name
                override val fullName: TextView = binding.fullName
                override val starBadge: InfoBadge = binding.starBadge
                override val langBadge: InfoBadge = binding.langBadge
            }
        }

        setVariation(
            variation = repoItemVariation {
                stateHandler {
                    drawName { view, state ->
                        view.name.text = state.name
                    }
                    drawFullName { view, state ->
                        view.fullName.text = state.fullName
                    }
                    drawStarBadgeState { view, state ->
                        view.starBadge.updateState { state.starBadgeState }
                    }
                    drawLangBadgeState { view, state ->
                        view.langBadge.updateState { state.langBadgeState }
                    }
                }
            }
        )

        requireViewStructure().apply {
            root.clicks()
                .onEach { dispatchAction(ViewAction.ClickAction(getState().id)) }
                .launchIn(viewScope)
        }
    }

    @io.gierla.rccore.annotations.State
    data class ViewState(
        val id: Long = 0L,
        val name: String = "",
        val fullName: String = "",
        val starBadgeState: InfoBadge.ViewState = InfoBadge.ViewState(),
        val langBadgeState: InfoBadge.ViewState = InfoBadge.ViewState()
    ) : State

    @io.gierla.rccore.annotations.Action
    sealed class ViewAction : Action {
        class ClickAction(val id: Long) : ViewAction()
    }

    @io.gierla.rccore.annotations.Structure
    interface ViewStructure : Structure {
        val root: ConstraintLayout
        val name: TextView
        val fullName: TextView
        val starBadge: InfoBadge
        val langBadge: InfoBadge
    }

}