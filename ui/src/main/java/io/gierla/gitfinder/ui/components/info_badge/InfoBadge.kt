package io.gierla.gitfinder.ui.components.info_badge

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import io.gierla.gitfinder.ui.R
import io.gierla.gitfinder.ui.databinding.InfoBadgeBinding
import io.gierla.gitfinder.ui.extensions.toPx
import io.gierla.gitfinder.ui.helper.ReactiveConstraintLayout
import io.gierla.rccore.annotations.ReactiveComponent
import io.gierla.rccore.main.action.Action
import io.gierla.rccore.main.state.State
import io.gierla.rccore.views.view.Structure
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@ReactiveComponent
class InfoBadge @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ReactiveConstraintLayout<InfoBadge.ViewState, InfoBadge.ViewAction, InfoBadge.ViewStructure>(
    ViewState(),
    context,
    attrs,
    defStyleAttr
) {

    private var binding: InfoBadgeBinding

    init {
        inflate(context, R.layout.info_badge, this)

        binding = InfoBadgeBinding.bind(rootView)

        attrs?.let {
            context.obtainStyledAttributes(attrs, R.styleable.InfoBadge).apply {
                if (hasValue(R.styleable.InfoBadge_icon)) {
                    binding.icon.visibility = View.VISIBLE
                    binding.icon.setImageDrawable(getDrawable(R.styleable.InfoBadge_icon))
                } else {
                    binding.icon.visibility = View.GONE
                }
                recycle()
            }
        }

        setViewStructure {
            object : ViewStructure {
                override val root: ConstraintLayout = this@InfoBadge
                override val text: TextView = binding.text
            }
        }

        setVariation(
            variation = infoBadgeVariation {
                stateHandler {
                    drawText { view, state ->
                        view.text.text = state.text
                    }
                }
            }
        )

        requireViewStructure().apply {
            root.background = ContextCompat.getDrawable(context, R.drawable.star_badge_background)
            root.setPadding(14.toPx(), 4.toPx(), 14.toPx(), 4.toPx())
        }
    }

    @io.gierla.rccore.annotations.State
    data class ViewState(
        val text: String = ""
    ) : State

    @io.gierla.rccore.annotations.Action
    sealed class ViewAction : Action {
    }

    @io.gierla.rccore.annotations.Structure
    interface ViewStructure : Structure {
        val root: ConstraintLayout
        val text: TextView
    }

}