package io.gierla.gitfinder.ui.helper

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import io.gierla.rccore.main.action.Action
import io.gierla.rccore.main.state.State
import io.gierla.rccore.views.store.DefaultReactiveView
import io.gierla.rccore.views.store.ReactiveView
import io.gierla.rccore.views.view.Structure
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel

@ExperimentalCoroutinesApi
abstract class ReactiveConstraintLayout<S: State, A: Action, V: Structure> @JvmOverloads constructor(
    initialState: S,
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(
    context,
    attributeSet,
    defStyleAttr
), ReactiveView<S, A, V> by DefaultReactiveView<S, A, V>(initialState) {

    protected var viewScope: CoroutineScope = CoroutineScope(Dispatchers.Main)

    override fun onDetachedFromWindow() {
        viewScope.cancel()
        detachView()
        super.onDetachedFromWindow()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        viewScope = CoroutineScope(Dispatchers.Main)
        attachView()
    }

}