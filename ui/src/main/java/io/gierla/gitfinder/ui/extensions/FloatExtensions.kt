package io.gierla.gitfinder.ui.extensions

import android.content.res.Resources

fun Float.toPx(): Float = (this * Resources.getSystem().displayMetrics.density)