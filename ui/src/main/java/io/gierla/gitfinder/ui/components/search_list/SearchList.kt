package io.gierla.gitfinder.ui.components.search_list

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.gierla.gitfinder.ui.R
import io.gierla.gitfinder.ui.components.repo_item.RepoItem
import io.gierla.gitfinder.ui.extensions.toPx
import io.gierla.gitfinder.ui.helper.ReactiveRecyclerView
import io.gierla.rccore.annotations.ReactiveComponent
import io.gierla.rccore.main.action.Action
import io.gierla.rccore.views.view.Structure
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@ReactiveComponent
class SearchList @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ReactiveRecyclerView<SearchList.ViewState, SearchList.ViewAction, SearchList.ViewStructure>(
    ViewState(),
    context,
    attrs,
    defStyleAttr
) {

    private val viewAdapter = SearchListAdapter { dispatchAction(ViewAction.RepoItemAction(it)) }

    init {
        setHasFixedSize(true)
        layoutManager = LinearLayoutManager(context)

        addItemDecoration(ItemDecorator())

        viewAdapter.addLoadStateListener { loadState ->
            val isLoading = loadState.source.refresh is LoadState.Loading

            val isEmpty = loadState.append.endOfPaginationReached && (adapter?.itemCount ?: 0) < 1

            val error = (loadState.source.refresh as? LoadState.Error ?: loadState.source.append as? LoadState.Error ?: loadState.source.prepend as? LoadState.Error ?: loadState.append as? LoadState.Error ?: loadState.prepend as? LoadState.Error)

            val hasError = error != null
            val errorMessage = error?.error?.localizedMessage ?: context.getString(R.string.generic_error)
            dispatchAction(ViewAction.LoadErrorAction(isLoading, isEmpty, hasError, errorMessage))
        }
        adapter = viewAdapter.withLoadStateFooter(SearchListLoadAdapter())

        setViewStructure {
            object : ViewStructure {
                override val root: RecyclerView = this@SearchList
                override val adapter: SearchListAdapter = viewAdapter
            }
        }

        setVariation(
            variation = searchListVariation {
                stateHandler {
                    drawRepoItems { view, state ->
                        viewScope.launch {
                            view.adapter.submitData(state.repoItems)
                        }
                    }
                }
            }
        )
    }

    private class ItemDecorator : ItemDecoration() {
        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: State) {
            val position = parent.getChildAdapterPosition(view)

            val topSpace = if (position == 0) {
                20.toPx()
            } else {
                0
            }

            outRect.run {
                top = topSpace
                bottom = 10.toPx()
                left = 20.toPx()
                right = 20.toPx()
            }
        }
    }

    @io.gierla.rccore.annotations.State
    data class ViewState(
        val repoItems: PagingData<RepoItem.ViewState> = PagingData.empty()
    ) : io.gierla.rccore.main.state.State

    @io.gierla.rccore.annotations.Action
    sealed class ViewAction : Action {
        class LoadErrorAction(val isLoading: Boolean, val isEmpty: Boolean, val hasError: Boolean, val error: String = "") : ViewAction()
        class RepoItemAction(val repoItemAction: RepoItem.ViewAction) : ViewAction()
    }

    @io.gierla.rccore.annotations.Structure
    interface ViewStructure : Structure {
        val root: RecyclerView
        val adapter: SearchListAdapter
    }

}