package io.gierla.gitfinder.ui.screens

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import io.gierla.gitfinder.ui.R
import io.gierla.gitfinder.ui.components.repo_item.RepoItem
import io.gierla.gitfinder.ui.components.screen_error.ScreenError
import io.gierla.gitfinder.ui.components.search_list.SearchList
import io.gierla.gitfinder.ui.databinding.SearchScreenBinding
import io.gierla.gitfinder.ui.extensions.toPx
import io.gierla.gitfinder.ui.helper.ReactiveConstraintLayout
import io.gierla.rccore.annotations.ReactiveComponent
import io.gierla.rccore.main.action.Action
import io.gierla.rccore.main.action.ActionListener
import io.gierla.rccore.main.state.State
import io.gierla.rccore.views.view.Structure
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import reactivecircus.flowbinding.android.widget.textChanges

@OptIn(FlowPreview::class)
@ExperimentalCoroutinesApi
@ReactiveComponent
class SearchScreen @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ReactiveConstraintLayout<SearchScreen.ViewState, SearchScreen.ViewAction, SearchScreen.ViewStructure>(
    ViewState(),
    context,
    attrs,
    defStyleAttr
) {

    private var binding: SearchScreenBinding

    init {
        inflate(context, R.layout.search_screen, this)

        binding = SearchScreenBinding.bind(rootView)

        setViewStructure {
            object : ViewStructure {
                override val root: ConstraintLayout = this@SearchScreen
                override val searchList: SearchList = binding.searchList
                override val searchInput: EditText = binding.searchInput
                override val loadingIndicator: ProgressBar = binding.loadingIndicator
                override val error: ScreenError = binding.error
                override val emptyContainer: ConstraintLayout = binding.emptyContainer
            }
        }

        setVariation(
            variation = searchScreenVariation {
                stateHandler {
                    drawSearchListState { view, state ->
                        view.searchList.updateState { state.searchListState }
                    }
                    drawIsLoading { view, state ->
                        view.loadingIndicator.visibility = if (state.isLoading) View.VISIBLE else View.GONE
                    }
                    drawHasError { view, state ->
                        view.error.visibility = if (state.hasError) View.VISIBLE else View.GONE
                    }
                    drawScreenErrorState { view, state ->
                        view.error.updateState { state.screenErrorState }
                    }
                    drawIsEmpty { view, state ->
                        view.emptyContainer.visibility = if (state.isEmpty) View.VISIBLE else View.GONE
                    }
                }
            }
        )

        requireViewStructure().apply {
            root.setBackgroundColor(Color.WHITE)
            root.setPadding(0, 40.toPx(), 0, 0)

            searchInput
                .textChanges()
                .skipInitialValue()
                .debounce(1000)
                .onEach {
                    dispatchAction(ViewAction.SearchAction(it.toString()))
                }
                .launchIn(viewScope)

            error.setActionListener { action -> dispatchAction(ViewAction.ScreenErrorAction(action)) }
            searchList.setActionListener { action -> dispatchAction(ViewAction.SearchListAction(action)) }
        }
    }

    @io.gierla.rccore.annotations.State
    data class ViewState(
        val searchListState: SearchList.ViewState = SearchList.ViewState(),
        val isLoading: Boolean = false,
        val hasError: Boolean = false,
        val isEmpty: Boolean = true,
        val screenErrorState: ScreenError.ViewState = ScreenError.ViewState()
    ) : State

    @io.gierla.rccore.annotations.Action
    sealed class ViewAction : Action {
        class SearchAction(val search: String) : ViewAction()
        class SearchListAction(val searchListAction: SearchList.ViewAction) : ViewAction()
        class ScreenErrorAction(val screenErrorAction: ScreenError.ViewAction) : ViewAction()
    }

    @io.gierla.rccore.annotations.Structure
    interface ViewStructure : Structure {
        val root: ConstraintLayout
        val searchList: SearchList
        val searchInput: EditText
        val loadingIndicator: ProgressBar
        val error: ScreenError
        val emptyContainer: ConstraintLayout
    }

}