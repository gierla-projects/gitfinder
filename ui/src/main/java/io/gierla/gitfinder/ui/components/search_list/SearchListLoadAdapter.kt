package io.gierla.gitfinder.ui.components.search_list

import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import io.gierla.gitfinder.ui.R
import io.gierla.gitfinder.ui.components.error_loading_item.ErrorLoadingItem
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class SearchListLoadAdapter : LoadStateAdapter<SearchListLoadAdapter.LoadViewHolder>() {

    override fun onBindViewHolder(holder: LoadViewHolder, loadState: LoadState) {
        when (loadState) {
            LoadState.Loading -> {
                holder.errorLoadingItem.setState {
                    ErrorLoadingItem.ViewState(
                        isLoading = true,
                        hasError = false,
                        error = ""
                    )
                }
            }
            is LoadState.Error -> {
                holder.errorLoadingItem.setState {
                    ErrorLoadingItem.ViewState(
                        isLoading = false,
                        hasError = true,
                        error = loadState.error.localizedMessage ?: holder.errorLoadingItem.context.getString(R.string.generic_error)
                    )
                }
            }
            is LoadState.NotLoading -> {
                holder.errorLoadingItem.setState {
                    ErrorLoadingItem.ViewState(
                        isLoading = false,
                        hasError = false,
                        error = ""
                    )
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadViewHolder {
        val errorLoadingItem = ErrorLoadingItem(parent.context)
        errorLoadingItem.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        errorLoadingItem.id = ViewCompat.generateViewId()
        return LoadViewHolder(errorLoadingItem)
    }

    class LoadViewHolder(val errorLoadingItem: ErrorLoadingItem) : RecyclerView.ViewHolder(errorLoadingItem)

}