package io.gierla.gitfinder.ui.components.search_list

import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.gierla.gitfinder.ui.components.repo_item.RepoItem
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class SearchListAdapter(private val repoItemActionListener: (repoItemAction: RepoItem.ViewAction) -> Unit) : PagingDataAdapter<RepoItem.ViewState, SearchListAdapter.RepoItemViewHolder>(COMPARATOR) {
    override fun onBindViewHolder(holder: RepoItemViewHolder, position: Int) {
        holder.repoItem.setActionListener { action -> repoItemActionListener.invoke(action) }
        getItem(position)?.let { state ->
            holder.repoItem.setState { state }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoItemViewHolder {
        val repoItem = RepoItem(parent.context)
        repoItem.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        repoItem.id = ViewCompat.generateViewId()
        return RepoItemViewHolder(repoItem)
    }

    class RepoItemViewHolder(val repoItem: RepoItem) : RecyclerView.ViewHolder(repoItem)

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<RepoItem.ViewState>() {
            override fun areItemsTheSame(oldItem: RepoItem.ViewState, newItem: RepoItem.ViewState): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: RepoItem.ViewState, newItem: RepoItem.ViewState): Boolean {
                return oldItem == newItem
            }
        }
    }
}