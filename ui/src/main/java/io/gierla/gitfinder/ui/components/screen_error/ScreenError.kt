package io.gierla.gitfinder.ui.components.screen_error

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import io.gierla.gitfinder.ui.R
import io.gierla.gitfinder.ui.databinding.ScreenErrorBinding
import io.gierla.gitfinder.ui.databinding.SearchScreenBinding
import io.gierla.gitfinder.ui.extensions.toPx
import io.gierla.gitfinder.ui.helper.ReactiveConstraintLayout
import io.gierla.rccore.annotations.ReactiveComponent
import io.gierla.rccore.main.action.Action
import io.gierla.rccore.main.state.State
import io.gierla.rccore.views.view.Structure
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import reactivecircus.flowbinding.android.view.clicks

@ExperimentalCoroutinesApi
@ReactiveComponent
class ScreenError @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ReactiveConstraintLayout<ScreenError.ViewState, ScreenError.ViewAction, ScreenError.ViewStructure>(
    ViewState(),
    context,
    attrs,
    defStyleAttr
) {

    private val binding: ScreenErrorBinding

    init {
        inflate(context, R.layout.screen_error, this)

        binding = ScreenErrorBinding.bind(rootView)

        setViewStructure {
            object : ViewStructure {
                override val root: ConstraintLayout = this@ScreenError
                override val errorField: TextView = binding.errorField
            }
        }

        setVariation(
            variation = screenErrorVariation {
                stateHandler {
                    drawError { view, state ->
                        view.errorField.text = state.error
                    }
                }
            }
        )

        requireViewStructure().apply {
            root.alpha = 0.95f
            root.setPadding(15.toPx(), 15.toPx(), 15.toPx(), 15.toPx())
            root.setBackgroundColor(Color.parseColor("#EFEFEF"))

            root.clicks()
                .onEach { dispatchAction(ViewAction.DismissAction) }
                .launchIn(viewScope)
        }
    }

    @io.gierla.rccore.annotations.State
    data class ViewState(
        val error: String = ""
    ) : State

    @io.gierla.rccore.annotations.Action
    sealed class ViewAction : Action {
        object DismissAction : ViewAction()
    }

    @io.gierla.rccore.annotations.Structure
    interface ViewStructure : Structure {
        val root: ConstraintLayout
        val errorField: TextView
    }

}