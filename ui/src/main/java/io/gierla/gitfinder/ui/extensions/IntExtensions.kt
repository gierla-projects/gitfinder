package io.gierla.gitfinder.ui.extensions

import android.content.res.Resources

fun Int.toPx(): Int = ((this * Resources.getSystem().displayMetrics.density).toInt())
