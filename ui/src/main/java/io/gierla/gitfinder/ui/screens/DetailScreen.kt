package io.gierla.gitfinder.ui.screens

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import coil.load
import coil.transform.CircleCropTransformation
import io.gierla.gitfinder.ui.R
import io.gierla.gitfinder.ui.components.info_badge.InfoBadge
import io.gierla.gitfinder.ui.components.screen_error.ScreenError
import io.gierla.gitfinder.ui.databinding.DetailScreenBinding
import io.gierla.gitfinder.ui.helper.ReactiveConstraintLayout
import io.gierla.rccore.annotations.ReactiveComponent
import io.gierla.rccore.main.action.Action
import io.gierla.rccore.main.state.State
import io.gierla.rccore.views.view.Structure
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import reactivecircus.flowbinding.android.view.clicks

@ExperimentalCoroutinesApi
@ReactiveComponent
class DetailScreen @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ReactiveConstraintLayout<DetailScreen.ViewState, DetailScreen.ViewAction, DetailScreen.ViewStructure>(
    ViewState(),
    context,
    attrs,
    defStyleAttr
) {

    private val binding: DetailScreenBinding

    init {
        inflate(context, R.layout.detail_screen, this)

        binding = DetailScreenBinding.bind(rootView)

        setViewStructure {
            object : ViewStructure {
                override val root: ConstraintLayout = this@DetailScreen
                override val error: ScreenError = binding.error
                override val title: TextView = binding.title
                override val subTitle: TextView = binding.subTitle
                override val description: TextView = binding.description
                override val backButton: AppCompatImageView = binding.backButton
                override val browserButton: AppCompatButton = binding.browserButton
                override val starBadge: InfoBadge = binding.starBadge
                override val langBadge: InfoBadge = binding.langBadge
                override val profileImage: AppCompatImageView = binding.profileImage
            }
        }

        setVariation(
            variation = detailScreenVariation {
                stateHandler {
                    drawHasError { view, state ->
                        view.error.visibility = if (state.hasError) View.VISIBLE else View.GONE
                    }
                    drawScreenErrorState { view, state ->
                        view.error.updateState { state.screenErrorState }
                    }
                    drawTitle { view, state ->
                        view.title.text = state.title
                    }
                    drawDescription { view, state ->
                        view.description.text = state.description
                    }
                    drawLangBadgeState { view, state ->
                        view.langBadge.updateState { state.langBadgeState }
                    }
                    drawStarBadgeState { view, state ->
                        view.starBadge.updateState { state.starBadgeState }
                    }
                    drawSubTitle { view, state ->
                        view.subTitle.text = state.subTitle
                    }
                    drawProfileImage { view, state ->
                        state.profileImage?.let { profileImage ->
                            view.profileImage.load(profileImage) {
                                transformations(CircleCropTransformation())
                            }
                            view.profileImage.visibility = View.VISIBLE
                        } ?: run {
                            view.profileImage.visibility = View.GONE
                        }
                    }
                }
            }
        )

        requireViewStructure().apply {
            root.setBackgroundColor(Color.WHITE)

            error.setActionListener { action -> dispatchAction(ViewAction.ScreenErrorAction(action)) }

            browserButton.clicks()
                .onEach {
                    getState().url?.let { url ->
                        dispatchAction(ViewAction.OpenBrowserAction(url))
                    }
                }
                .launchIn(viewScope)

            backButton.clicks()
                .onEach { dispatchAction(ViewAction.BackAction) }
                .launchIn(viewScope)
        }
    }

    @io.gierla.rccore.annotations.State
    data class ViewState(
        val title: String = "",
        val subTitle: String = "",
        val description: String = "",
        val url: String? = null,
        val profileImage: String? = null,
        val hasError: Boolean = false,
        val screenErrorState: ScreenError.ViewState = ScreenError.ViewState(),
        val starBadgeState: InfoBadge.ViewState = InfoBadge.ViewState(),
        val langBadgeState: InfoBadge.ViewState = InfoBadge.ViewState()
    ) : State

    @io.gierla.rccore.annotations.Action
    sealed class ViewAction : Action {
        class ScreenErrorAction(val screenErrorAction: ScreenError.ViewAction) : ViewAction()
        object BackAction : ViewAction()
        class OpenBrowserAction(val url: String) : ViewAction()
    }

    @io.gierla.rccore.annotations.Structure
    interface ViewStructure : Structure {
        val root: ConstraintLayout
        val error: ScreenError
        val title: TextView
        val subTitle: TextView
        val description: TextView
        val backButton: AppCompatImageView
        val browserButton: AppCompatButton
        val starBadge: InfoBadge
        val langBadge: InfoBadge
        val profileImage: AppCompatImageView
    }

}