package io.gierla.gitfinder.ui.components.error_loading_item

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import io.gierla.gitfinder.ui.R
import io.gierla.gitfinder.ui.databinding.ErrorLoadingItemBinding
import io.gierla.gitfinder.ui.extensions.toPx
import io.gierla.gitfinder.ui.helper.ReactiveConstraintLayout
import io.gierla.rccore.annotations.ReactiveComponent
import io.gierla.rccore.main.action.Action
import io.gierla.rccore.main.state.State
import io.gierla.rccore.views.view.Structure
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@ReactiveComponent
class ErrorLoadingItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ReactiveConstraintLayout<ErrorLoadingItem.ViewState, ErrorLoadingItem.ViewAction, ErrorLoadingItem.ViewStructure>(
    ViewState(),
    context,
    attrs,
    defStyleAttr
) {

    private val binding: ErrorLoadingItemBinding

    init {
        inflate(context, R.layout.error_loading_item, this)

        binding = ErrorLoadingItemBinding.bind(rootView)

        setViewStructure {
            object : ViewStructure {
                override val root: ConstraintLayout = this@ErrorLoadingItem
                override val progressBar: ProgressBar = binding.progressBar
                override val errorField: TextView = binding.errorField
                override val errorIcon: AppCompatImageView = binding.errorIcon
            }
        }

        setVariation(
            variation = errorLoadingItemVariation {
                stateHandler {
                    drawError { view, state ->
                        view.errorField.text = state.error
                    }
                    drawHasError { _, _ ->
                        //view.errorField.visibility = if (state.hasError) View.VISIBLE else View.GONE
                        //view.errorIcon.visibility = if (state.hasError) View.VISIBLE else View.GONE
                    }
                    drawIsLoading { view, state ->
                        view.progressBar.visibility = if (state.isLoading) View.VISIBLE else View.GONE
                    }
                }
            }
        )

        requireViewStructure().apply {
            root.setPadding(0, 15.toPx(), 0, 15.toPx())
        }
    }

    @io.gierla.rccore.annotations.State
    data class ViewState(
        val isLoading: Boolean = false,
        val hasError: Boolean = false,
        val error: String = ""
    ) : State

    @io.gierla.rccore.annotations.Action
    sealed class ViewAction : Action {
    }

    @io.gierla.rccore.annotations.Structure
    interface ViewStructure : Structure {
        val root: ConstraintLayout
        val progressBar: ProgressBar
        val errorField: TextView
        val errorIcon: AppCompatImageView
    }

}